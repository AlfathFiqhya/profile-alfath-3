from django.shortcuts import render

def profile(request):
    return render(request, 'profile.html')

def about(request):
    return render(request, 'about.html')

# Create your views here.
